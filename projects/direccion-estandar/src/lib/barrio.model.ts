import { NombreAbreviatura, Repositorio } from './direccion-estandar.model';

export class Barrio extends NombreAbreviatura {
}

Repositorio.agregar(Barrio, [
    {
        nombre: 'Barrio ',
        abreviatura: 'BR'
    },
    {
        nombre: 'Ciudadela',
        abreviatura: 'CD'
    },
    {
        nombre: 'Supermanzana',
        abreviatura: 'SM'
    }
]);