import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Complemento } from './complemento.model';
import { compararAbreviaturas, Repositorio } from './direccion-estandar.model';

export function RepositorioFactory() {
    return new Repositorio<Complemento>(Complemento);
}

@Component({
    selector: 'direccion-estandar-complemento',
    templateUrl: `./complemento.component.html`,
    providers: [
        { provide: Repositorio, useFactory: RepositorioFactory }
    ]
})
export class ComplementoComponent implements OnInit {

    @Input() formulario: FormGroup;
    @Input() value: string;

    constructor(private repositorio: Repositorio<Complemento>) { }

    ngOnInit(): void {
    }

    get Complementos(): Array<Complemento> {
        return this.repositorio.all();
    }

    comparador(a: Complemento, b: Complemento):  boolean {
        return compararAbreviaturas(a, b);
    }
}