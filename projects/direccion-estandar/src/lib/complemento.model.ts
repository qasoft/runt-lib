import { NombreAbreviatura, Repositorio } from './direccion-estandar.model';

export class Complemento extends NombreAbreviatura {
}

Repositorio.agregar(Complemento, [
    {
        nombre: 'Administracion',
        abreviatura: 'AD'
    },
    {
        nombre: 'Agrupación',
        abreviatura: 'AG'
    },
    {
        nombre: 'Altillo',
        abreviatura: 'AL'
    },
    {
        nombre: 'Apartamento',
        abreviatura: 'AP'
    },
    {
        nombre: 'Barrio',
        abreviatura: 'BR'
    },
    {
        nombre: 'Bloque',
        abreviatura: 'BQ'
    },
    {
        nombre: 'Bodega',
        abreviatura: 'BG'
    },
    {
        nombre: 'Casa',
        abreviatura: 'CS'
    },
    {
        nombre: 'Celula',
        abreviatura: 'CU'
    },
    {
        nombre: 'Centro Comercial',
        abreviatura: 'CE'
    },
    {
        nombre: 'Ciudadela',
        abreviatura: 'CD'
    },
    {
        nombre: 'Conjunto Residencial',
        abreviatura: 'CO'
    },
    {
        nombre: 'Consultorio',
        abreviatura: 'CN'
    },
    {
        nombre: 'Deposito',
        abreviatura: 'DP'
    },
    {
        nombre: 'Deposito Sotáno',
        abreviatura: 'DS'
    },
    {
        nombre: 'Edificio',
        abreviatura: 'ED'
    },
    {
        nombre: 'Entrada',
        abreviatura: 'EN'
    },
    {
        nombre: 'Esquina',
        abreviatura: 'EQ'
    },
    {
        nombre: 'Etapa',
        abreviatura: 'ET'
    },
    {
        nombre: 'Estacion',
        abreviatura: 'ES'
    },
    {
        nombre: 'Estación Transmilenio',
        abreviatura: 'TM'
    },
    {
        nombre: 'Exterior',
        abreviatura: 'EX'
    },
    {
        nombre: 'Finca',
        abreviatura: 'FI'
    },
    {
        nombre: 'Garaje',
        abreviatura: 'GA'
    },
    {
        nombre: 'Garaje Sotano',
        abreviatura: 'GS'
    },
    {
        nombre: 'Interior',
        abreviatura: 'IN'
    },
    {
        nombre: 'Kilometro',
        abreviatura: 'KM'
    },
    {
        nombre: 'Local',
        abreviatura: 'LC'
    },
    {
        nombre: 'Local Mezzanine',
        abreviatura: 'LM'
    },
    {
        nombre: 'Lote',
        abreviatura: 'LT'
    },
    {
        nombre: 'Manzana',
        abreviatura: 'MZ'
    },
    {
        nombre: 'Mezzanine',
        abreviatura: 'MN'
    },
    {
        nombre: 'Modulo',
        abreviatura: 'MD'
    },
    {
        nombre: 'Oficina',
        abreviatura: 'OF'
    },
    {
        nombre: 'Paradero de Transporte',
        abreviatura: 'PR'
    },
    {
        nombre: 'Parque',
        abreviatura: 'PQ'
    },
    {
        nombre: 'Parqueadero',
        abreviatura: 'PA'
    },
    {
        nombre: 'Pent-House',
        abreviatura: 'PN'
    },
    {
        nombre: 'Piso',
        abreviatura: 'PI'
    },
    {
        nombre: 'Planta',
        abreviatura: 'PL'
    },
    {
        nombre: 'Predio',
        abreviatura: 'PD'
    },
    {
        nombre: 'Portería',
        abreviatura: 'PR'
    },
    {
        nombre: 'Poste',
        abreviatura: 'PO'
    },
    {
        nombre: 'Puesto',
        abreviatura: 'PU'
    },
    {
        nombre: 'Salón',
        abreviatura: 'SA'
    },
    {
        nombre: 'Semáforo',
        abreviatura: 'SF'
    },
    {
        nombre: 'Round Point (Glorieta)',
        abreviatura: 'RP'
    },
    {
        nombre: 'Semisótano',
        abreviatura: 'SS'
    },
    {
        nombre: 'Sótano',
        abreviatura: 'SO'
    },
    {
        nombre: 'Sector',
        abreviatura: 'SC'
    },
    {
        nombre: 'Suite',
        abreviatura: 'ST'
    },
    {
        nombre: 'Supermanzana',
        abreviatura: 'SM'
    },
    {
        nombre: 'Telefono Público',
        abreviatura: 'TP'
    },
    {
        nombre: 'Terraza',
        abreviatura: 'TZ'
    },
    {
        nombre: 'Torre',
        abreviatura: 'TO'
    },
    {
        nombre: 'Unidad',
        abreviatura: 'UN'
    },
    {
        nombre: 'Unidad Residencial',
        abreviatura: 'UL'
    },
    {
        nombre: 'Urbanización',
        abreviatura: 'UR'
    },
    {
        nombre: 'Zona',
        abreviatura: 'ZN'
    }
]);