import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { compararAbreviaturas, Repositorio } from './direccion-estandar.model';
import { Urbanizacion } from './urbanizacion.model';

export function RepositorioFactory() {
    return new Repositorio<Urbanizacion>(Urbanizacion);
}

@Component({
    selector: 'direccion-estandar-urbanizacion',
    templateUrl: `./urbanizacion.component.html`,
    providers: [
        { provide: Repositorio, useFactory: RepositorioFactory }
    ]
})
export class UrbanizacionComponent implements OnInit {

    @Input() formulario: FormGroup;

    constructor(private repositorio: Repositorio<Urbanizacion>) { }

    ngOnInit(): void {
    }

    get Urbanizaciones(): Array<Urbanizacion> {
        return this.repositorio.all();
    }

    comparador(a: Urbanizacion, b: Urbanizacion): boolean {
        return compararAbreviaturas(a, b);
    }
}