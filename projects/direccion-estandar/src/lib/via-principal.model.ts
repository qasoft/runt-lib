import { NombreAbreviatura, Repositorio } from './direccion-estandar.model';

export class TipoVia extends NombreAbreviatura {

}

export class Prefijo extends NombreAbreviatura {
}

Repositorio.agregar(TipoVia, [
    {
        nombre: 'No Aplica',
        abreviatura: 'NA'
    },
    {
        nombre: 'Autopista',
        abreviatura: 'AU'
    },
    {
        nombre: 'Avenida',
        abreviatura: 'AV'
    },
    {
        nombre: 'Avenida Calle',
        abreviatura: 'AC'
    },
    {
        nombre: 'Avenida Carrera',
        abreviatura: 'AK'
    },
    {
        nombre: 'Bulevar',
        abreviatura: 'BL'
    },
    {
        nombre: 'Calle',
        abreviatura: 'CL'
    },
    {
        nombre: 'Carrera',
        abreviatura: 'KR'
    },
    {
        nombre: 'Carretera',
        abreviatura: 'CT'
    },
    {
        nombre: 'Circular',
        abreviatura: 'CQ'
    },
    {
        nombre: 'Circunvalar',
        abreviatura: 'CV'
    },
    {
        nombre: 'Cuentas Corridas',
        abreviatura: 'CC'
    },
    {
        nombre: 'Diagonal',
        abreviatura: 'DG'
    },
    {
        nombre: 'Pasaje',
        abreviatura: 'PJ'
    },
    {
        nombre: 'Paseo',
        abreviatura: 'PS'
    },
    {
        nombre: 'Peatonal',
        abreviatura: 'PT'
    },
    {
        nombre: 'Transversal',
        abreviatura: 'TV'
    },
    {
        nombre: 'Troncal',
        abreviatura: 'TC'
    },
    {
        nombre: 'Variante',
        abreviatura: 'VT'
    },
    {
        nombre: 'Vía',
        abreviatura: 'VI'
    }
]);

Repositorio.agregar(Prefijo, [
    {
        nombre: 'Bis',
        abreviatura: 'BIS'
    }
]);