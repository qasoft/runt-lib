import { NombreAbreviatura, Repositorio } from './direccion-estandar.model';

export class Manzana extends NombreAbreviatura {
}

Repositorio.agregar(Manzana, [
    {
        nombre: 'Manzana',
        abreviatura: 'MZ'
    },
    {
        nombre: 'Interior',
        abreviatura: 'IN'
    },
    {
        nombre: 'Sector',
        abreviatura: 'CS'
    },
    {
        nombre: 'Etapa',
        abreviatura: 'ET'
    },
    {
        nombre: 'Edificio',
        abreviatura: 'ED'
    },
    {
        nombre: 'Modulo',
        abreviatura: 'MD'
    },
    {
        nombre: 'Torre',
        abreviatura: 'TO'
    }
]);