import { DireccionEstandarFormulario } from './direccion-estandar.formulario';
import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
    selector: 'runt-direccion-estandar-modal',
    templateUrl: './direccion-estandar-modal-content.component.html'
})
export class DireccionEstandarModalContentComponent implements OnInit {
    ayuda: boolean = false;
    formulario: DireccionEstandarFormulario;

    constructor(public activeModal: NgbActiveModal) { }

    ngOnInit(): void {
        this.formulario = new DireccionEstandarFormulario();
    }

    onToggleAyuda(): void {
        this.ayuda = !this.ayuda;
    }

    onValidar(): void {
        if (this.formulario.valid) {
            this.activeModal.close(this.formulario);
        }
    }

    onLimpiar(): void {
        this.formulario.reset();
    }

    onCerrar(): void {
        this.activeModal.close();
    }
}
