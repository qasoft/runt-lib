import { NombreAbreviatura, Repositorio } from './direccion-estandar.model';

export class Urbanizacion extends NombreAbreviatura {
}

Repositorio.agregar(Urbanizacion, [
    {
        nombre: 'Bloque',
        abreviatura: 'BQ'
    },
    {
        nombre: 'Celula',
        abreviatura: 'CU'
    },
    {
        nombre: 'Conjunto Residencial',
        abreviatura: 'CO'
    },
    {
        nombre: 'Etapa',
        abreviatura: 'ET'
    },
    {
        nombre: 'Urbanizacion',
        abreviatura: 'UR'
    },
    {
        nombre: 'Sector',
        abreviatura: 'SC'
    },
    {
        nombre: 'Torre',
        abreviatura: 'TO'
    },
    {
        nombre: 'Zona',
        abreviatura: 'ZN'
    }
]);