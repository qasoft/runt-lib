import { NombreAbreviatura, Repositorio } from './direccion-estandar.model';

export class TipoPredio extends NombreAbreviatura {
}

Repositorio.agregar(TipoPredio, [
    {
        nombre: 'Altillo',
        abreviatura: 'AL'
    },
    {
        nombre: 'Apartamento',
        abreviatura: 'AP'
    },
    {
        nombre: 'Bodega',
        abreviatura: 'BG'
    },
    {
        nombre: 'Casa',
        abreviatura: 'CS'
    },
    {
        nombre: 'Consultorio',
        abreviatura: 'CN'
    },
    {
        nombre: 'Deposito',
        abreviatura: 'DP'
    },
    {
        nombre: 'Deposito Sotano',
        abreviatura: 'DS'
    },
    {
        nombre: 'Garaje',
        abreviatura: 'GA'
    },
    {
        nombre: 'Garaje Sotano',
        abreviatura: 'GS'
    },
    {
        nombre: 'Local',
        abreviatura: 'LC'
    },
    {
        nombre: 'Local Mezzanine',
        abreviatura: 'LM'
    },
    {
        nombre: 'Lote',
        abreviatura: 'LT'
    },
    {
        nombre: 'Mezzanine',
        abreviatura: 'MN'
    },
    {
        nombre: 'Oficina',
        abreviatura: 'OF'
    },
    {
        nombre: 'Parqueadero',
        abreviatura: 'PA'
    },
    {
        nombre: 'Pent-House',
        abreviatura: 'PN'
    },
    {
        nombre: 'Planta',
        abreviatura: 'PL'
    },
    {
        nombre: 'Predio',
        abreviatura: 'PD'
    },
    {
        nombre: 'Semisótano',
        abreviatura: 'SS'
    },
    {
        nombre: 'Sótano',
        abreviatura: 'SO'
    },
    {
        nombre: 'Suite',
        abreviatura: 'ST'
    },
    {
        nombre: 'Terraza',
        abreviatura: 'TZ'
    },
    {
        nombre: 'Unidad',
        abreviatura: 'UN'
    },
    {
        nombre: 'Unidad Residencial',
        abreviatura: 'UL'
    }
]);