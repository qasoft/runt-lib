import { DireccionEstandarDTO } from './direccion-estandar.model';
import { FormGroup } from '@angular/forms';
import { Component, Input } from '@angular/core';

@Component({
    selector: 'runt-direccion-estandar-ayuda',
    templateUrl: './direccion-estandar-ayuda.component.html'
})
export class DireccionEstandarAyudaComponent {
    private ejemploUno: DireccionEstandarDTO = { viaPrincipal: { tipo: { abreviatura: 'CL' }, nombreONumero: '25' }, viaGeneradora: { nombre: '32', letraVia: 'A', numeroPlaca: '58', cuadrante: { abreviatura: 'SUR' } }, barrio: { nombre: 'EJEMPLO', tipo: { abreviatura: 'BR' }} };
    private ejemploDos: DireccionEstandarDTO = { viaPrincipal: { tipo: { abreviatura: 'CL' }, nombreONumero: '25', letraVia: 'A', prefijo: { abreviatura: 'BIS' }, cuadrante: { abreviatura: 'SUR' } }, viaGeneradora: { nombre: '32', letraVia: 'A', numeroPlaca: '58' }, barrio: { nombre: 'EJEMPLO', tipo: { abreviatura: 'BR' }}, urbanizacion: { nombre: 'ONE', tipo: { abreviatura: 'CO' }}, manzana: { nombre: '1', tipo: { abreviatura: 'IN' }}, predio: { nombre: '502', tipo: { abreviatura: 'AP'} } };

    @Input() formulario: FormGroup;

    ejemploUnoActivo: boolean = false;
    ejemploDosActivo: boolean = false;
    valor: DireccionEstandarDTO;

    onEjemploUno(): void {
        this.retenerValorActual();
        this.formulario.disable();

        this.formulario.reset(this.ejemploUno);

        this.ejemploUnoActivo = true;
        this.ejemploDosActivo = false;
    }

    onEjemploDos(): void {
        this.retenerValorActual();
        this.formulario.disable();

        this.formulario.reset(this.ejemploDos);

        this.ejemploDosActivo = true;
        this.ejemploUnoActivo = false;
    }

    onRestablecer(): void {
        this.formulario.enable();
        this.formulario.reset(this.valor || {}, { emitEvent: true, onlySelf: false });
                
        this.ejemploUnoActivo = false;
        this.ejemploDosActivo = false;
    }

    private retenerValorActual() {
        if (!this.ejemploUnoActivo && !this.ejemploDosActivo) {
            this.valor = this.formulario.value;
        }
    }
}