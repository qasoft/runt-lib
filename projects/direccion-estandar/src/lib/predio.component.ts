import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { compararAbreviaturas, Repositorio } from './direccion-estandar.model';
import { TipoPredio } from './predio.model';

export function RepositorioFactory() {
    return new Repositorio<TipoPredio>(TipoPredio);
}

@Component({
    selector: 'direccion-estandar-predio',
    templateUrl: `./predio.component.html`,
    providers: [
        { provide: Repositorio, useFactory: RepositorioFactory }
    ]
})
export class PredioComponent implements OnInit {

    @Input() formulario: FormGroup;

    constructor(private repositorio: Repositorio<TipoPredio>) { }

    ngOnInit(): void {

    }

    get TiposPredio(): Array<TipoPredio> {
        return this.repositorio.all();
    }

    comparador(a: TipoPredio, b: TipoPredio): boolean {
        return compararAbreviaturas(a, b);
    }
}