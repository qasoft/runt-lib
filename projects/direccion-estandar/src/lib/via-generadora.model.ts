import { NombreAbreviatura, Repositorio } from './direccion-estandar.model';

export class Sufijo extends NombreAbreviatura {
}

Repositorio.agregar(Sufijo, [
    {
        nombre: 'Bis',
        abreviatura: 'BIS'
    }
]);