import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { compararAbreviaturas, Repositorio } from './direccion-estandar.model';
import { Manzana } from './manzana.model';

export function RepositorioFactory() {
    return new Repositorio<Manzana>(Manzana);
}

@Component({
    selector: 'direccion-estandar-manzana',
    templateUrl: `./manzana.component.html`,
    providers: [
        { provide: Repositorio, useFactory: RepositorioFactory }
    ]
})
export class ManzanaComponent implements OnInit {

    @Input() formulario: FormGroup;

    constructor(private repositorio: Repositorio<Manzana>) { }

    ngOnInit(): void {
    }

    get Manzanas(): Array<Manzana> {
        return this.repositorio.all();
    }

    comparador(a: Manzana, b: Manzana): boolean {
        return compararAbreviaturas(a, b);
    }
}