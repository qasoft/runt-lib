export interface MessageFnResolver {
    of(key: string): MessageFn;
}

export declare type MessageFn = (error: any) => string;